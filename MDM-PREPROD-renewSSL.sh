#!/bin/sh
echo "arret tomcat"
 service tomcat9 stop
echo "Renouvellement des certificats"
/usr/bin/certbot renew
echo "Redémarrage TOMCAT" 
service tomcat9 start
echo "Recopie des certificats dans Tomcat"
cp /etc/letsencrypt/live/mdm*.crm.weka-ssc.fr/*.pem /etc/tomcat9/
echo "Fix des droits"
chown root:tomcat /etc/tomcat9/*.pem
chmod 644 /etc/tomcat9/*.pem